import logo from './logo.svg';
import './App.css';
import { stringify } from 'querystring';

function App() {
  return (
    <div className="App">
      <button onClick={()=>{
        var msg = "";
        for (let index = 0; index < 1024 * 1024 * 50; index++) {
          msg+="0";
        }
        if(!window.ReactNativeWebView) {alert("No app detected :-("); return;}
        alert("Press ok to send message to react-native (size:"+msg.length+")...");
        window.ReactNativeWebView.postMessage(msg);
      }}>Send message to react native</button>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
